﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Lunar
{
    class Camera
    {
        Viewport view;
        public Matrix transform;
        Vector2 center;

        public Camera(Viewport view)
        {
            this.view = view;
        }

        public void Update(GameTime gameTime, Character character)
        {
            center = new Vector2(character.position.X + (character.body.Width / 2) - 400, character.position.Y + (character.body.Height / 2) - 250);
            //ship.spritePosition.Y + (ship.spriteRectangle.Height / 2 - 250);
            transform = Matrix.CreateScale(new Vector3(1, 1, 0)) * Matrix.CreateTranslation(new Vector3(-center.X, -center.Y, 0));

        }
    }
}
