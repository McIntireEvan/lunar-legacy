﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Lunar
{
    class HUDitem
    {
        Texture2D sprite;
        public int currentFrame = 0;
        public int totalFrames;

        public Vector2 position;

        int height;
        int width;

        public HUDitem(int columns, Texture2D texture)
        {
            totalFrames = columns; // Set the Total frames to the amount of columns it is contructed with
            sprite = texture; // sets the sprite
            height = 60; // Set height
            width = 60; // set width
        }

        public void Update()
        {

        }

        public void Draw(SpriteBatch spriteBatch, Vector2 position)
        {
            Rectangle sourceRectangle = new Rectangle(width * currentFrame, 0, width, height); // Get Source From Spritesheet
            Rectangle destinationRectangle = new Rectangle((int)position.X, (int)position.Y, width, height); // Set Destination on Window

            spriteBatch.Draw(sprite, destinationRectangle, sourceRectangle, Color.White); // Draws it

        }
    }
}
