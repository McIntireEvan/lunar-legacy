﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Lunar
{
    class HUD
    {
        public float oxyLevel = 1050;
        public float armorLevel = 100;

        Texture2D oxygenTex;
        Texture2D suitTex;

        HUDitem oxygen;
        HUDitem armor;

        SpriteBatch spriteBatch;

        public void LoadContent()
        {
            
        }

        public HUD(Texture2D oxy, Texture2D suit, SpriteBatch spriteBatchIn)
        {
            oxygenTex = oxy;
            suitTex = suit;

            spriteBatch = spriteBatchIn;
            oxygen = new HUDitem(11, oxygenTex); // Makes oxygen HUDitem
            armor = new HUDitem(11, suitTex); // Makes armor HUDitem
        }

        public void updateArmorLevel()
        {
            if (armorLevel == 100)
            {
                armor.currentFrame = 0;
                
            }
            else if (armorLevel >= 90 && armorLevel < 100)
            {
                armor.currentFrame = 1;
            }
            else if (armorLevel >= 80 && armorLevel < 90)
            {
                armor.currentFrame = 2;
            }
            else if (armorLevel >= 70 && armorLevel < 80)
            {
                armor.currentFrame = 3;
            }
            else if (armorLevel >= 60 && armorLevel < 70)
            {
                armor.currentFrame = 4;
            }
            else if (armorLevel >= 50 && armorLevel < 60)
            {
                armor.currentFrame = 5;
            }
            else if (armorLevel >= 40 && armorLevel < 50)
            {
                armor.currentFrame = 6;
            }
            else if (armorLevel >= 30 && armorLevel < 40)
            {
                armor.currentFrame = 7;
            }
            else if (armorLevel >= 20 && armorLevel < 30)
            {
                armor.currentFrame = 8;
            }
            else if (armorLevel >= 10 && armorLevel < 20)
            {
                armor.currentFrame = 9;
            }
            else if (armorLevel > 0 && armorLevel < 10)
            {
                armor.currentFrame = 10;
            }
            else if (armorLevel == 0)
            {
                armor.currentFrame = 11;
            }
        } // Updates Armor Sprite Based on the level

        public void updateOxyLevel()
        {
            if (oxyLevel >= 1000)
            {
                oxygen.currentFrame = 0;
            }
            else if (oxyLevel >= 900 && oxyLevel < 1000)
            {
                oxygen.currentFrame = 1;
            }
            else if (oxyLevel >= 800 && oxyLevel < 900)
            {
                oxygen.currentFrame = 2;
            }
            else if (oxyLevel >= 700 && oxyLevel < 800)
            {
                oxygen.currentFrame = 3;
            }
            else if (oxyLevel >= 600 && oxyLevel < 700)
            {
                oxygen.currentFrame = 4;
            }
            else if (oxyLevel >= 500 && oxyLevel < 600)
            {
                oxygen.currentFrame = 5;
            }
            else if (oxyLevel >= 400 && oxyLevel < 500)
            {
                oxygen.currentFrame = 6;
            }
            else if (oxyLevel >= 300 && oxyLevel < 400)
            {
                oxygen.currentFrame = 7;
            }
            else if (oxyLevel >= 200 && oxyLevel < 300)
            {
                oxygen.currentFrame = 8;
            }
            else if (oxyLevel >= 100 && oxyLevel < 200)
            {
                oxygen.currentFrame = 9;
            }
            else if (oxyLevel > 0 && oxyLevel < 100)
            {
                oxygen.currentFrame = 10;
            }
            else if (oxyLevel == 0)
            {
                oxygen.currentFrame = 11;
            }
        } // Updates Oxy Sprite based on its level

        public void update()
        {
            if (oxyLevel != 0) // If thet are not dead, update the oxy level
            {
                oxyLevel = oxyLevel - (1 / armorLevel);
            }

        }

        public void Draw(SpriteBatch spriteBatch, Vector2 position)
        {
            oxygen.position = new Vector2(0,0); // Sets the Oxy Sprite Position
            armor.position = new Vector2(60,0); // Sets the Armor Sprite Positon
            oxygen.Draw(spriteBatch, oxygen.position); // Draws Oxy Level Sprite
            armor.Draw(spriteBatch, armor.position); // Draws Armror Level Sprite
        }
    }
}
