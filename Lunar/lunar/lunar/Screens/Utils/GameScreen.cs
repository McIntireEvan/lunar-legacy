﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Lunar
{
    public enum ScreenState
    {
        Hidden,
        Visible,
        Active,
        Off
    }

    public class GameScreen
    {
        //Public Variables/Objects
        public ScreenState state;
        public bool isMainScreen = true;

        //Protected Variables/Objects
        protected ContentManager content;

        //Private Variables/Objects
        private ScreenHandler screenHandler;
        private GraphicsDevice graphics;

        #region Properties

        public ScreenHandler ScreenHandler
        {
            get { return screenHandler; }
            internal set { screenHandler = value; }
        }

        public GraphicsDevice GraphicsDevice
        {
            get { return graphics; }
            internal set { graphics = value; }
        }

        #endregion

        #region Initialization

        public virtual void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenHandler.Game.Services, "Content");
        }

        public virtual void UnloadContent()
        {
            content.Unload();
        }

        #endregion

        #region Update and Draw

        public virtual void Update(GameTime gameTime)
        {
            if (state == ScreenState.Off)
            {
                ScreenHandler.RemoveScreen(this);
            }
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch) { }

        #endregion
    }
}