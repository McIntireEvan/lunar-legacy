﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Lunar
{
    public class ScreenHandler : Microsoft.Xna.Framework.DrawableGameComponent
    {
        //ScreenHandler
        List<GameScreen> screens = new List<GameScreen>();
        List<GameScreen> screensToBeAdded = new List<GameScreen>();
        List<GameScreen> screensToBeRemoved = new List<GameScreen>();
        public GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        bool isInit = false;
        List<Type> types = new List<Type>();
        Game1 game;

        public SpriteBatch SpriteBatch//Common SpriteBatch
        {
            get { return spriteBatch; }
        }

        public ScreenHandler(Game1 game, GraphicsDeviceManager g)
            : base(game)
        {
            this.game = game;
            graphics = g;
        }

        public override void Initialize()
        {
            base.Initialize();
            isInit = true;
            AddScreen(new MainMenu(graphics, game));
        }

        protected override void LoadContent()
        {
            ContentManager content = Game.Content;
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        public void AddScreen(GameScreen screen)
        {
            //Check if we have one already
            Type screenType = screen.GetType();
            if (types.Contains(screenType) == false)
            {
                types.Add(screenType);
                //Set it up
                screen.ScreenHandler = this;
                screen.GraphicsDevice = graphics.GraphicsDevice;
                screen.state = ScreenState.Active;
                //Load it
                if (isInit)
                {
                    screen.LoadContent();
                }
                //Add it
                screensToBeAdded.Add(screen);
            }
        }

        public void RemoveScreen(GameScreen screen)
        {
            if (isInit)
            {
                screen.UnloadContent();
            }
            //Allow future screens of this type
            Type screenType = screen.GetType();
            types.Remove(screenType);
            //Remove the screen
            screens.Remove(screen);
        }

        public void RemoveScreen()
        {
            //Find screens of this type
            Type screenType;
            bool foundType = false;
            for (int i = 0; i < screens.Count; i++)
            {
                screenType = screens[i].GetType();
                //if (screens[i] is LobbyScreen)
                //{ foundType = true; screensToBeRemoved.Add(screens[i]); }
            }
        }

        public override void Update(GameTime gameTime)
        {
            foreach (GameScreen screen in screens)
            {
                if (screen.state == ScreenState.Active)
                {
                    screen.Update(gameTime);
                }
                else if (screen.state == ScreenState.Off)
                {
                    screensToBeRemoved.Add(screen);
                }
            }
            //Add Screens
            screens.AddRange(screensToBeAdded);
            screensToBeAdded.Clear();

            //Dispose Off Screens
            foreach (GameScreen screen in screensToBeRemoved)
            {
                RemoveScreen(screen);
            }
            screensToBeRemoved.Clear();

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            foreach (GameScreen screen in screens)
            {
                if ((screen.state == ScreenState.Active) || (screen.state == ScreenState.Visible))
                {
                    screen.Draw(gameTime, spriteBatch);
                }
            }
            base.Draw(gameTime);
        }
    }
}