﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Lunar.Screens
{
    class Layer
    {
        Camera camera;

        public Layer(GraphicsDevice graphicsDevice)
        {
            camera = new Camera(graphicsDevice.Viewport);
        }

        public void Update(GameTime gameTime, Character character)
        {
            camera.Update(gameTime, character);
        }

        public void Draw(GameTime gameTile, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, camera.transform);

            spriteBatch.End();
        }
    }
}
