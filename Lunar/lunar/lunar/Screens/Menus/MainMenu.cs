﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Lunar
{
    public class MainMenu : GameScreen
    {
        GraphicsDeviceManager graphics;
        Game1 game;

        SpriteFont font;

        Rectangle playRect;
        Rectangle exitRect;

        Rectangle musicRect;
        Rectangle soundRect;

        Texture2D background;

        MouseState mState;

        Vector2 mousePos;

        Color dispColorPlay = Color.White;
        Color dispColorExit = Color.White;

        string playState = "";
        string exitState = "";

        Rectangle destinationRect;

        public static bool willExit = false;

        public MainMenu(GraphicsDeviceManager graphics, Game1 game)
        {
            this.game = game;
            this.graphics = graphics;
            this.Initialize();
        }

        public void Initialize()
        {
            playRect = new Rectangle(20, 50, 100, 40);
            exitRect = new Rectangle(20, 90, 100, 40);

            destinationRect = new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
         }

        public override void LoadContent()
        {
            base.LoadContent();

            font = content.Load<SpriteFont>("SpatiumFont_small");
            background = content.Load<Texture2D>("background");

            
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            mState = Mouse.GetState();

            mousePos.X = mState.X;
            mousePos.Y = mState.Y;

            if (playRect.Contains((int)mousePos.X, (int)mousePos.Y))
            {
                dispColorPlay = Color.LightBlue;
                playState = ">";

                if (mState.LeftButton.Equals(ButtonState.Pressed))
                {
                    game.pauseScreen = new PauseScreen(graphics, game);
                    game.gameScreen = new MainScreen(graphics, game);
                    ScreenHandler.AddScreen(game.gameScreen);
                    ScreenHandler.AddScreen(game.pauseScreen);
                    game.pauseScreen.state = ScreenState.Hidden;
                    this.state = ScreenState.Off;
                }
            }

            else
            {
                dispColorPlay = Color.White;
                playState = "";

            }

            if (exitRect.Contains((int)mousePos.X, (int)mousePos.Y))
            {
                dispColorExit = Color.LightBlue;
                exitState = ">";

                if (mState.LeftButton.Equals(ButtonState.Pressed))
                {
                    willExit = true;
                }
            }
            else
            {
                dispColorExit = Color.White;
                exitState = "";

            }

            
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();

            spriteBatch.Draw(background, destinationRect, Color.White);
            spriteBatch.DrawString(font, "LUNAR PRE ALPHA", Vector2.Zero, Color.White);
            spriteBatch.DrawString(font, playState + "  PLAY", new Vector2(30, 50), dispColorPlay);
            spriteBatch.DrawString(font, exitState + "  EXIT", new Vector2(30, 90), dispColorExit);
            

            spriteBatch.End();

            base.Draw(gameTime, spriteBatch);
        }
    }
}
