﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;

namespace Lunar
{
    public class PauseScreen : GameScreen
    {
        GraphicsDeviceManager graphics;

        SpriteFont font;

        Rectangle playRect;
        Rectangle exitRect;

         MouseState mState;

        Vector2 mousePos;

        Color dispColorPlay = Color.White;
        Color dispColorExit = Color.White;

        string playState = "";
        string exitState = "";

        Game1 game;

        public PauseScreen(GraphicsDeviceManager graphics, Game1 game)
        {
            this.game = game;
            this.graphics = graphics;
            this.Initialize();
        }

        public void Initialize()
        {
            playRect = new Rectangle(20, 50, 100, 40);
            exitRect = new Rectangle(20, 90, 100, 40);
        }

        public override void LoadContent()
        {
            base.LoadContent();

            font = content.Load<SpriteFont>("SpatiumFont_small");
            


        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            mState = Mouse.GetState();

            mousePos.X = mState.X;
            mousePos.Y = mState.Y;

            if (playRect.Contains((int)mousePos.X, (int)mousePos.Y))
            {
                dispColorPlay = Color.LightBlue;
                playState = ">";

                if (mState.LeftButton.Equals(ButtonState.Pressed))
                {
                    game.gameScreen.state = ScreenState.Active;
                    this.state = ScreenState.Hidden;   
                }
            }

            else
            {
                dispColorPlay = Color.White;
                playState = "";

            }

            if (exitRect.Contains((int)mousePos.X, (int)mousePos.Y))
            {
                dispColorExit = Color.LightBlue;
                exitState = ">";

                if (mState.LeftButton.Equals(ButtonState.Pressed))
                {
                    ScreenHandler.AddScreen(new MainMenu(graphics, game));
                    Thread.Sleep(500);
                    this.state = ScreenState.Off;
                }
            }
            else
            {
                dispColorExit = Color.White;
                exitState = "";

            }


        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();

            spriteBatch.DrawString(font, playState + "  RESUME", new Vector2(30, 50), dispColorPlay);
            spriteBatch.DrawString(font, exitState + "  MAIN MENU", new Vector2(30, 90), dispColorExit);

            spriteBatch.End();

            base.Draw(gameTime, spriteBatch);
        }
    }
}
