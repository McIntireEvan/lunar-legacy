﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;
using System.IO;
using System.Xml;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using Lunar.Screens;

namespace Lunar
{
    public class MainScreen : GameScreen
    {
        GraphicsDeviceManager graphics;
        Character test = new Character();
        RockThrower rockThrowerTest;
        Orb OrbTest;
        HUD hud;

        Texture2D[] rockThrower_left = new Texture2D[7];
        Texture2D[] rockThrower_right = new Texture2D[7];

        Texture2D orbTex;
        Texture2D oxygenTex;
        Texture2D suitTex;

        SpriteBatch spriteBatch;

        int millisecondsPerFrame = 100;
        int timeSinceLastUpdate = 0;

        Game1 game;

        List<Layer> layers = new List<Layer>();

        public MainScreen(GraphicsDeviceManager graphics, Game1 game)
        {
            this.game = game;
            this.graphics = graphics;
        }

        public override void LoadContent()
        {
            base.LoadContent();
            test.LoadContent(content);

            oxygenTex = content.Load<Texture2D>("HUD/OXYGEN");
            suitTex = content.Load<Texture2D>("HUD/SUIT");

            hud = new HUD(oxygenTex, suitTex, spriteBatch);
            hud.oxyLevel = 100;
            hud.armorLevel = 100;

            #region rockThrowerSprite
            /*for (int i = 0; i <= 6; i++)
            {
                rockThrower_left[i] = content.Load<Texture2D>("robots/Rock_Thrower/left/IDLE" + (i + 1));
                rockThrower_right[i] = content.Load<Texture2D>("robots/Rock_Thrower/right/IDLE" + (i + 1));
            }*/
            #endregion


            rockThrowerTest = new RockThrower(rockThrower_left);

            orbTex = content.Load<Texture2D>("robots/orb/ice/ice");
            OrbTest = new Orb(orbTex, Orb.Type.ice);         
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            
            test.Update(gameTime);
            hud.updateArmorLevel();
            hud.updateOxyLevel();

            timeSinceLastUpdate += (int)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (timeSinceLastUpdate >= millisecondsPerFrame)
            {
                #region animUpdates
                timeSinceLastUpdate = 0;

                rockThrowerTest.Update();
                hud.update();
                #endregion
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                game.pauseScreen.state = ScreenState.Active;
                this.state = ScreenState.Hidden;
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            

            test.Draw(gameTime, spriteBatch);
            TileManager.draw(spriteBatch);

            base.Draw(gameTime, spriteBatch);
            spriteBatch.End();
        }
    }
}