﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Lunar
{
    public class AnimatedSprite
    {
        public int currentFrame;
        public int totalFrames;

        public int height;
        public int width;

        public Texture2D sprite;

        int rows;
        int columns;
        Texture2D[] spriteA;
        public int currentSprite;
        public int totalSprites;

        public AnimatedSprite(int columns, int height, int width)
        {
            // Set the coums and total frames based on the construtor
            this.columns = columns;
            this.totalFrames = columns;
            this.height = height;
            this.width = width;
        }

        public AnimatedSprite(int rows, int columns, Texture2D[] spriteIn, int numSprites)
        {
            totalSprites = numSprites;
            spriteA = new Texture2D[spriteIn.Length];

            //load in sprites for the multi-spritesheet animations
            for (int i = 1; i <= spriteIn.Length - 1; i++)
            {
                spriteA[i] = spriteIn[i];
            }

            //Sets the amount of Rows and Colums Equal to those used to construct it
            this.rows = rows;
            this.columns = columns;

            //Initialize some stuff
            currentFrame = 0;
            totalFrames = rows * columns;
            currentSprite = 1;
        }

        public void Update()
        {
            //Add to the current frame, and if it reaches the max, reset it
            currentFrame++;
            if (currentFrame == totalFrames)
                currentFrame = 0; 
        }

        public void UpdateMulti()
        {
            // Adds to the current frames, and if it reaches the max, add one to the spritesheet and set the frame to 0
            currentFrame++;
            if (currentFrame == totalFrames)
            {
                currentFrame = 0;

                currentSprite++;
                if (currentSprite == totalSprites)
                    currentSprite = 1;
            }
        }

        public void UpdateJump()
        {
            //Add to the current frame, and if it reaches the max, reset it
            currentFrame++;
            if (currentFrame == totalFrames)
                currentFrame = totalFrames;
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 location)
        {
            int currColumn = currentFrame;
            Rectangle sourceRectangle = new Rectangle(width * currColumn, 0, width, height); //Get the Source Rectange from the Spritesheet
            Rectangle destinationRectangle = new Rectangle((int)location.X, (int)location.Y, width, height); //Set the Destination Rectangle

            //Draw it
            spriteBatch.Draw(sprite, destinationRectangle, sourceRectangle, Color.White);
        }

        public void DrawMulti(SpriteBatch spriteBatch, Vector2 position)
        {
            //Set width and height, and calculate the row and column based on the current fram and the width and Height

            int width = spriteA[1].Width / columns;
            int height = spriteA[1].Height / rows;
            int row = (int)((float)currentFrame / (float)columns);
            int column = currentFrame % columns;

            Rectangle sourcerectangle = new Rectangle(width * column, height * row, width, height); //Get the Source Rectange from the Spritesheet
            Rectangle destinationRectangle = new Rectangle((int)position.X, (int)position.Y, width, height);


            spriteBatch.Draw(spriteA[currentSprite], destinationRectangle, sourcerectangle, Color.White); //Set the Destination Rectangle
        }

    }
}