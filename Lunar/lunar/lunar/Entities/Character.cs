﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace Lunar
{
    public class Character
    {
        SpriteBatch spriteBatch;

        //Animation Textures
        Texture2D leftIdle;
        Texture2D rightIdle;

        Texture2D leftWalk;
        Texture2D rightWalk;

        Texture2D leftRun;
        Texture2D rightRun;

        Texture2D leftSkid;
        Texture2D rightSkid;

        Texture2D leftJump;
        Texture2D rightJump;

        //AnimatedSprite Declaring
        AnimatedSprite idle;
        AnimatedSprite walk;
        AnimatedSprite run;
        AnimatedSprite skid;
        AnimatedSprite jump;

        //Other shit
        SpriteFont font;
        public Vector2 position;
        public float velocity;
        bool isJumping;
        public float yVelocity;
        public float startY;
        bool isSkidding = false;

        float gravity = -14;

        int millisecondsPerFrame = 100;
        int timeSinceLastUpdate = 0;

        enum Side { left, right };

        enum State { idle, walk, run, skid, jump, fall };

        State state;

        Side side;
        Side lastSide;

        Rectangle rectangle;
        public Rectangle body;
        Rectangle legs;


        bool top = false, bottom = false, left = false, right = false;

        public void LoadContent(ContentManager Content)
        {
            //Load all the stuff in
            leftIdle = Content.Load<Texture2D>("player/left/idle");
            rightIdle = Content.Load<Texture2D>("player/right/idle");

            leftWalk = Content.Load<Texture2D>("player/left/walk");
            rightWalk = Content.Load<Texture2D>("player/right/walk");

            leftRun = Content.Load<Texture2D>("player/left/run");
            rightRun = Content.Load<Texture2D>("player/right/run");

            leftSkid = Content.Load<Texture2D>("player/left/skid");
            rightSkid = Content.Load<Texture2D>("player/right/skid");

            leftJump = Content.Load<Texture2D>("player/left/jump");
            rightJump = Content.Load<Texture2D>("player/right/jump");

            position = new Vector2(100, 300);

            //Declare Stuff
            idle = new AnimatedSprite(4, 150, 150);
            idle.sprite = leftIdle;

            walk = new AnimatedSprite(10, 150, 150);
            walk.sprite = leftWalk;

            run = new AnimatedSprite(8, 150, 150);
            run.sprite = leftRun;

            skid = new AnimatedSprite(3, 150, 150);
            skid.sprite = leftSkid;

            jump = new AnimatedSprite(10, 150, 150);
            jump.sprite = leftJump;

            side = Side.left;
        }

        public void animUpdate(GameTime gameTime)
        {
            //add mS to timeSinceLastUpdate
            timeSinceLastUpdate += gameTime.ElapsedGameTime.Milliseconds;

            //if 100mS have passed
            if (timeSinceLastUpdate > millisecondsPerFrame)
            {
                timeSinceLastUpdate = 0;

                //Update Based on state
                if (state.Equals(State.idle))
                    idle.Update();

                if (state.Equals(State.walk))
                    walk.Update();

                if (state.Equals(State.run))
                    run.Update();

                if (state.Equals(State.skid))
                    skid.Update();

                if (state.Equals(State.jump))
                {
                    jump.UpdateJump();
                }
            }
        }

        public void animDraw(SpriteBatch spriteBatch)
        {
            //draw based on state
            if (state.Equals(State.idle))
                idle.Draw(spriteBatch, position);

            if (state.Equals(State.walk))
                walk.Draw(spriteBatch, position);

            if (state.Equals(State.run))
                run.Draw(spriteBatch, position);

            if (state.Equals(State.skid))
                skid.Draw(spriteBatch, position);

            if (state.Equals(State.jump))
                jump.Draw(spriteBatch, position);
        }

        public void setSide()
        {
            //if the side is left, set all textures to the left one
            if (side.Equals(Side.left))
            {
                idle.sprite = leftIdle;
                walk.sprite = leftWalk;
                run.sprite = leftRun;
                skid.sprite = leftSkid;
                jump.sprite = leftJump;
                velocity = velocity - (velocity / 4);
            }

            //if the side is right, set all textures to the right one
            if (side.Equals(Side.right))
            {
                idle.sprite = rightIdle;
                walk.sprite = rightWalk;
                run.sprite = rightRun;
                skid.sprite = rightSkid;
                jump.sprite = rightJump;
                velocity = velocity - (velocity / 4);
            }
        }

        public void setState()
        {
            //Set the state based on conditions
            if (isJumping == false)
            {

                if (isSkidding == false)
                {
                    if (velocity == 0)
                    {
                        state = State.idle;
                    }

                    else if (velocity > 0 && velocity < 6)
                    {
                        state = State.walk;
                    }
                    else
                    {
                        state = State.run;
                    }
                }
                else
                {
                    state = State.skid;
                }
            }
            else
            {
                state = State.jump;
            }
        }

        public void Update(GameTime gameTime)
        {
            body = new Rectangle((int)this.position.X + 20, (int)this.position.Y + 15, 90, 95);
            legs = new Rectangle((int)this.position.X + 60, (int)this.position.Y + 110, 35, 40);

            foreach (Tile t in TileManager.tiles)
            {
                if (CollisionUtils.checkCollisions(t, this.body,CollisionCheckType.TOP, yVelocity) == true) { top = true; }

                if (CollisionUtils.checkCollisions(t, this.legs, CollisionCheckType.BOTTOM, yVelocity) == true)
                { 
                    bottom = true;
                    if (legs.Intersects(t.rect))
                    {
                        this.position = CollisionUtils.fixBottomIntersection(position);
                    }
                }

                if (CollisionUtils.checkCollisions(t, this.body,CollisionCheckType.LEFT, velocity) == true) { left = true; }

                if (CollisionUtils.checkCollisions(t, this.body, CollisionCheckType.RIGHT, velocity) == true) { right = true; }

                if (CollisionUtils.checkCollisions(t, this.legs, CollisionCheckType.LEFT, velocity) == true) { left = true; }

                if (CollisionUtils.checkCollisions(t, this.legs, CollisionCheckType.RIGHT, velocity) == true) { right = true; }
            }

            KeyboardState state = Keyboard.GetState();

            //Increase/Decrease Gravity
            if (state.IsKeyDown(Keys.OemPlus))
            {
                gravity += .5f;
            }

            if (state.IsKeyDown(Keys.Z))
            {
                this.position.X = 200;
                this.position.Y = 100;
            }

            if (state.IsKeyDown(Keys.OemMinus))
            {
                gravity -= .5f;
            }

            if (state.IsKeyDown(Keys.E))
            {
                foreach (Tile t in TileManager.tiles)
                {
                    t.toggleDoor();
                }
                System.Threading.Thread.Sleep(500);
            }

            if (isJumping)
            {
                //Add the amount of yVelocity to our y position, and decrease the velocity
                if (top)
                {
                    position.Y++;
                    yVelocity = 0;
                }
                position.Y += yVelocity;

                yVelocity += .5f;

                //if the Y pos gets to be the start pos then we stop jumping
                if (bottom)
                {
                    isJumping = false;
                }
            }
            else
            {
                if (state.IsKeyDown(Keys.Space))
                {
                    //Set jumping to true, set the yVelocity to how much gravity we have, set the startY and reset the jump animation
                    isJumping = true;
                    yVelocity = gravity;
                    startY = position.Y;
                    jump.currentFrame = 0;
                }
                if (!bottom && isJumping == false)
                {
                    isJumping = true;
                    yVelocity = 0;
                }
            }

            if (isSkidding == true)
            {
                //If the side is left move left
                if (side.Equals(Side.left))
                    if (!left)
                        position.X -= velocity;

                //If the side is right move righht
                if (side.Equals(Side.right))
                    if (!right)
                        position.X += velocity;

                //Slow down
                velocity -= .2f;

                //if the velocity reaches 0 we stop skidding
                if ((velocity <= 0))
                {
                    velocity = 0;
                    isSkidding = false;
                }

            }
            else
            {

                if (state.IsKeyDown(Keys.A))
                {
                    //If we are not skidding, we will increase the velocity if it is not more than 10, move to the left, and set the side
                    if (isSkidding == false)
                    {
                        if (velocity <= 10)
                            velocity += .05f;

                        if (!left)
                            position.X -= velocity;
                        side = Side.left;
                    }
                }

                else if (state.IsKeyDown(Keys.D))
                {
                    //If we are not skidding, we will increase the velocity if it is not more than 10, move to the right, and set the side
                    if (isSkidding == false)
                    {
                        if (velocity <= 10)
                            velocity += .05f;
                        if (!right)
                            position.X += velocity;
                        side = Side.right;
                    }
                }

                else if (state.IsKeyDown(Keys.S))
                {
                    //If the velocity is greater than 1 we want to skid
                    if (isJumping == false)
                        if (velocity > 1)
                            isSkidding = true;
                }

                else
                {
                    //Set Velocity to 0 so we dont get the velocity to be -0.38535385
                    if (velocity < 0)
                        velocity = 0;

                    //If we are not skidding
                    if (isSkidding == false)
                    {
                        //If the velocity is more than 0
                        if (velocity > 0)
                        {
                            //Slow down
                            velocity -= .15f;

                            //Keep moving based on side
                            if (side.Equals(Side.left))
                                if (!left)
                                    position.X -= velocity;

                            if (side.Equals(Side.right))
                                if (!right)
                                    position.X += velocity;
                        }
                    }
                }
            }

            //If the side is the same as the last side, we want to swap textures
            if (side == lastSide)
            {
                setSide();
            }

            //Set lastSide based on side
            if (side.Equals(Side.left))
                lastSide = Side.right;

            if (side.Equals(Side.right))
                lastSide = Side.left;

            //Update and set the state
            setState();
            animUpdate(gameTime);

            top = false;
            bottom = false;
            left = false;
            right = false;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            animDraw(spriteBatch);
        }
    }

}