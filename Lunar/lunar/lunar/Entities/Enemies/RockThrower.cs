﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;
using System.IO;

namespace Lunar
{
    class RockThrower
    {
        public Vector2 pos;
        Texture2D[] idleSprite = new Texture2D[7];
        AnimatedSprite idleAnim;

        public RockThrower(Texture2D[] textureIn)
        {
            for (int i = 0; i <= textureIn.Length - 1; i++) // Loads all the spritesheets
            {
                idleSprite[i] = textureIn[i];
            }
            idleAnim = new AnimatedSprite(4, 4, idleSprite, 7); // Intits Idle Anim
            pos.X = 300; // Position X
            pos.Y = 300; // Position Y
        }

        public void Update()
        {
            idleAnim.UpdateMulti(); // Updates the Idle Anim
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 position)
        {
            idleAnim.DrawMulti(spriteBatch, position); // Draws the Idle Anim
        }

    }
}
