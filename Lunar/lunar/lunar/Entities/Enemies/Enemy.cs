﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Lunar
{
    abstract class Enemy
    {
        public Vector2 position;

        public Texture2D leftIdle;
        public Texture2D leftWalk;
        public Texture2D leftRun;
        public Texture2D leftHurt;
        public Texture2D leftAttack;
        public Texture2D leftJump;

        public Texture2D rightIdle;
        public Texture2D rightWalk;
        public Texture2D rightRun;
        public Texture2D rightHurt;
        public Texture2D rightAttack;
        public Texture2D rightJump;

        public AnimatedSprite idle;
        public AnimatedSprite walk;
        public AnimatedSprite run;
        public AnimatedSprite hurt;
        public AnimatedSprite attack;
        public AnimatedSprite jump;

        int timeSinceLastUpdate = 0;
        int millisecondsPerFrame = 50;

        enum Side { left, right };

        enum State { idle, walk, run, skid, jump };

        State state;

        Side side;
        Side lastSide;

        public void animUpdate(GameTime gameTime)
        {
            //add mS to timeSinceLastUpdate
            timeSinceLastUpdate += gameTime.ElapsedGameTime.Milliseconds;

            //if 100mS have passed
            if (timeSinceLastUpdate > millisecondsPerFrame)
            {
                timeSinceLastUpdate = 0;

                //Update Based on state
                if (state.Equals(State.idle))
                    idle.Update();

                if (state.Equals(State.walk))
                    walk.Update();

                if (state.Equals(State.run))
                    run.Update();

                if (state.Equals(State.jump))
                {
                    jump.UpdateJump();
                }
            }
        }

        public void animDraw(SpriteBatch spriteBatch)
        {
            //draw based on state
            if (state.Equals(State.idle))
                idle.Draw(spriteBatch, position);

            if (state.Equals(State.walk))
                walk.Draw(spriteBatch, position);

            if (state.Equals(State.run))
                run.Draw(spriteBatch, position);

            if (state.Equals(State.jump))
                jump.Draw(spriteBatch, position);
        }

        /*public void setSide()
        {
            //if the side is left, set all textures to the left one
            if (side.Equals(Side.left))
            {
                idle.sprite = leftIdle;
                walk.sprite = leftWalk;
                run.sprite = leftRun;
                jump.sprite = leftJump;
            }

            //if the side is right, set all textures to the right one
            if (side.Equals(Side.right))
            {
                idle.sprite = rightIdle;
                walk.sprite = rightWalk;
                run.sprite = rightRun;
                jump.sprite = rightJump;
            }
        }*/

        public void Update(GameTime gameTime)
        {
            //If the side is the same as the last side, we want to swap textures
            if (side == lastSide)
            {
                //setSide();
            }

            //Set lastSide based on side
            if (side.Equals(Side.left))
                lastSide = Side.right;

            if (side.Equals(Side.right))
                lastSide = Side.left;

            //Update and set the state
            animUpdate(gameTime);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            animDraw(spriteBatch);
        }

    }
}