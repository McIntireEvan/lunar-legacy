﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Lunar
{
    public enum CollisionCheckType {TOP, BOTTOM, LEFT, RIGHT }

    public static class CollisionUtils
    {
        public static bool checkCollisions(Tile t, Rectangle rect, CollisionCheckType type)
        {
            switch (type)
            {
                case CollisionCheckType.BOTTOM:
                    {
                        rect.Y += 2;
                        break;
                    }
                case CollisionCheckType.TOP:
                    {
                        rect.Y -= 2;
                        break;
                    }
                case CollisionCheckType.LEFT:
                    {
                        rect.X -= 2;
                        break;
                    }
                case CollisionCheckType.RIGHT:
                    {
                        rect.X += 2;
                        break;
                    }
            }

            return t.Intersects(rect);
        }

        public static bool checkCollisions(Tile t, Rectangle rect, CollisionCheckType type, float velocity)
        {
            switch (type)
            {
                case CollisionCheckType.BOTTOM:
                    {
                        rect.Y += (int)velocity;
                        break;
                    }
                case CollisionCheckType.TOP:
                    {
                        rect.Y -= (int)velocity;
                        break;
                    }
                case CollisionCheckType.LEFT:
                    {
                        rect.X -= (int)velocity;
                        break;
                    }
                case CollisionCheckType.RIGHT:
                    {
                        rect.X += (int)velocity;
                        break;
                    }
            }

            return t.Intersects(rect);
        }

        public static Vector2 fixBottomIntersection(Vector2 position)
        {
            position.Y--;
            return position;
        }
    }
}