﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.IO;

namespace Lunar
{
    public enum TileType { NORMAL, ACTIVEDOOR, INACTIVEDOOR, DAMAGE, HEAL, OXYGEN, SPACE };

    public class Tile
    {
        public int indexX;
        public int indexY;
        int height = 16;
        int width = 16;
        public TileType type;

        Texture2D spritesheet;

        public Rectangle rect;

        bool isSolid;

        public Tile(Texture2D spritesheet, TileType type, int indexX, int indexY)
        {
            this.indexX = indexX;
            this.indexY = indexY;
            this.type = type;
            this.spritesheet = spritesheet;
            rect = new Rectangle(indexX * width, indexY * height, width, height);

            switch (type)
            {
                case TileType.NORMAL:
                    {
                        this.isSolid = true;
                        break;
                    }
                case TileType.ACTIVEDOOR:
                    {
                        this.isSolid = true;
                        break;
                    }
                case TileType.DAMAGE:
                    {
                        this.isSolid = false;
                        break;
                    }
                case TileType.HEAL:
                    {
                        this.isSolid = false;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        public void update(GameTime gameTime)
        {

        }

        public bool Intersects(Rectangle rect)
        {
            if (this.rect.Intersects(rect) && (this.isSolid == true)) { return true; }

            return false;
        }

        public void toggleDoor()
        {
            if (this.type.Equals(TileType.ACTIVEDOOR))
            {
                this.type = TileType.INACTIVEDOOR;
                isSolid = false;
            }
            else if (this.type.Equals(TileType.INACTIVEDOOR))
            {
                this.type = TileType.ACTIVEDOOR;
                isSolid = true;
            }

        }

        public void draw(SpriteBatch spriteBatch)
        {
            int index = 0;
            if (type == TileType.NORMAL)
            {
                index = 0;
            }
            if (type == TileType.HEAL)
            {
                index = 1;
            }
            if (type == TileType.ACTIVEDOOR)
            {
                index = 2;
            }
            if (type == TileType.INACTIVEDOOR)
            {
                index = 1;
            }
            if (type == TileType.DAMAGE)
            {
                index = 3;
            }
            Rectangle sourcerectangle = new Rectangle(index * 16, 0, 16, 16); //Get the Source Rectange from the Spritesheet
            Rectangle destinationRectangle = new Rectangle((indexX * width), (indexY * height), width, height);

            spriteBatch.Draw(spritesheet, destinationRectangle, sourcerectangle, Color.White);
        }
    }
}