﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace Lunar
{
    public static class TileManager
    {
        public static List<Tile> tiles = new List<Tile>();

        public static void draw(SpriteBatch spriteBatch)
        {
            foreach (Tile t in tiles)
            {
                t.draw(spriteBatch);
            }
        }
    }
}
