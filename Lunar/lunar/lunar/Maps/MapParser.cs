﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using System.IO;

namespace Lunar
{
    static class MapParser
    {
        public static void parseMap(List<List<char>> map, Texture2D tiles)
        {
            int indexX = 0;
            int indexY = 0;
            Console.WriteLine("[Begin Reading Map]");
            foreach (List<char> row in map)
            {
                Console.Write("[Reading Row]");
                foreach (char c in row)
                {
                    Console.Write(c);
                    switch (c)
                    {
                        //Red
                        case '#':
                            {
                                TileManager.tiles.Add(new Tile(tiles, TileType.NORMAL, indexX, indexY));
                                indexX++;
                                
                                break;
                            }

                        //Blue
                        case '%':
                            {
                                TileManager.tiles.Add(new Tile(tiles, TileType.ACTIVEDOOR, indexX, indexY));
                                indexX++;
                                
                                break;
                            }
                        //Green
                        case '&':
                            {
                                TileManager.tiles.Add(new Tile(tiles, TileType.NORMAL, indexX, indexY));
                                indexX++;
                                
                                break;
                            }
                        //Yellow
                        case '/':
                            {
                                TileManager.tiles.Add(new Tile(tiles, TileType.NORMAL, indexX, indexY));
                                indexX++;
                                
                                break;
                            }
                        case '.':
                            {
                                indexX++;
                                
                                break;
                            }
                    }
                }
                indexY++;
                indexX = 0;

            }
            Console.WriteLine("[Finished reading map]");
        }
        public static List<List<char>> readMap(String path)
        {
            List<List<char>> map = new List<List<char>>();
            using (StreamReader reader = new StreamReader(path))
            {
                String line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<char> mapLine = new List<char>();
                    foreach (char c in line)
                    {
                        mapLine.Add(c);
                    }
                    map.Add(mapLine);
                }
            }
            return map;
        }

        public static void setUpMap(String path, Texture2D tiles)
        {
            parseMap(readMap(path), tiles);
        }
    }
}
